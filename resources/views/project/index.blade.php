<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <link rel="shortcut icon" href="./assets/img/favicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/creativetimofficial/tailwind-starter-kit/compiled-tailwind.min.css" />
    <title>Portofolio</title>
</head>

<body class="text-gray-800 antialiased">
    <nav class="top-0 absolute z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 ">
        <div class="container px-4 mx-auto flex flex-wrap items-center justify-between">
            <div class="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
                <a class="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white" href="/">Portofolio</a><button class="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none" type="button" onclick="toggleNavbar('example-collapse-navbar')">
                    <i class="text-white fas fa-bars"></i>
                </button>
            </div>
            <div class="lg:flex flex-grow items-center bg-white lg:bg-transparent lg:shadow-none hidden" id="example-collapse-navbar">
                <ul class="flex flex-col lg:flex-row list-none lg:ml-auto">
                    <li class="flex items-center">
                        <a class="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold" href="/"><span class="inline-block ml-2">Home</span></a>
                    </li>
                    <li class="flex items-center">
                        <a class="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold" href="/about"><span class="inline-block ml-2">About</span></a>
                    </li>
                    <li class="flex items-center">
                        <a class="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold" href="/education"><span class="inline-block ml-2">Education</span></a>
                    </li>
                    <li class="flex items-center">
                        <a class="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold" href="/project"><span class="inline-block ml-2">Projects</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main>
        <section class="absolute w-full h-full">
            <div class="absolute top-0 w-full h-full bg-gray-900" style="background-image: url(https://raw.githubusercontent.com/creativetimofficial/tailwind-starter-kit/main/Login%20Page/html-login-page/assets/img/register_bg_2.png); background-size: 100%; background-repeat: no-repeat;">
            </div>
            <div class="container mx-auto px-6 h-full">
                <div class="flex content-center items-center justify-center h-full">
                    <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                        <div class="rounded-t mb-0 px-3 py-1">
                            <div class="flex flex-wrap mt-4">
                                <div class="w-full xl:w-8/12 mb-12 xl:mb-0">
                                    <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
                                        <div class="rounded-t mb-0 px-4 py-3 border-0">
                                            <div class="flex flex-wrap items-center">
                                                <div class="relative w-full px-4 max-w-full flex-grow flex-1">
                                                    <h3 class="font-semibold text-base text-blueGray-700">
                                                        Projects
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block w-full overflow-x-auto">
                                            <table class="items-center w-full bg-transparent border-collapse">
                                                <thead>
                                                    <tr>
                                                        <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                                            Year
                                                        </th>
                                                        <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                                            Role
                                                        </th>
                                                        <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                                            Project
                                                        </th>
                                                        <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                                            Description
                                                        </th>
                                                        <th class="px-3 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                                                            Action
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($projectDatas as $projectItem)
                                                    <tr>
                                                        <th class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                                                            {{ $projectItem->year }}
                                                        </th>
                                                        <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                            {{ $projectItem->role }}
                                                        </td>
                                                        <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                            {{ $projectItem->project }}
                                                        </td>
                                                        <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                            {{ $projectItem->description }}
                                                        </td>
                                                        <td class="border-t-0 px-2 border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                            <form action="{{ route('project.destroy',$projectItem->id) }}" method="Post">
                                                                <a href="{{ route('project.edit',$projectItem->id) }}" class="bg-blue-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-2 py-1 rounded outline-none focus:outline-none mr-1 mb-1" style="transition:all .15s ease">
                                                                    Edit
                                                                </a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button class="bg-red-500 text-white active:bg-indigo-600 text-xs font-bold uppercase px-2 py-1 rounded outline-none focus:outline-none mr-1 mb-1" type="submit" style="transition:all .15s ease">
                                                                    Delete
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="w-full xl:w-4/12 px-4">
                                    <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
                                        <div class="block w-full px-4 py-3">
                                            <form action="{{ route('project.store') }}" method="POST" enctype="multipart/form-data">
                                                @csrf
                                                <div class="relative w-full mb-3">
                                                    <label class="block uppercase text-gray-700 text-xs font-bold mb-2">Year</label>
                                                    <input type="text" name="year" id="year-form" class="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full" placeholder="Year" style="transition: all 0.15s ease 0s;" />
                                                </div>
                                                @error('year')
                                                <div role="alert">
                                                    <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-2 py-2 text-red-700">
                                                        <p>{{ $message }}</p>
                                                    </div>
                                                </div>
                                                @enderror
                                                <div class="relative w-full mb-3">
                                                    <label class="block uppercase text-gray-700 text-xs font-bold mb-2">Role</label>
                                                    <input type="text" name="role" id="role-form" class="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full" placeholder="role" style="transition: all 0.15s ease 0s;" />
                                                </div>
                                                @error('role')
                                                <div role="alert">
                                                    <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-2 py-2 text-red-700">
                                                        <p>{{ $message }}</p>
                                                    </div>
                                                </div>
                                                @enderror
                                                <div class="relative w-full mb-3">
                                                    <label class="block uppercase text-gray-700 text-xs font-bold mb-2">Project</label>
                                                    <input type="text" name="project" id="project-form" class="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full" placeholder="project" style="transition: all 0.15s ease 0s;" />
                                                </div>
                                                @error('project')
                                                <div role="alert">
                                                    <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-2 py-2 text-red-700">
                                                        <p>{{ $message }}</p>
                                                    </div>
                                                </div>
                                                @enderror
                                                <div class="relative w-full mb-3">
                                                    <label class="block uppercase text-gray-700 text-xs font-bold mb-2">description</label>
                                                    <input type="text" name="description" id="description-form" class="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full" placeholder="description" style="transition: all 0.15s ease 0s;" />
                                                </div>
                                                @error('description')
                                                <div role="alert">
                                                    <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-2 py-2 text-red-700">
                                                        <p>{{ $message }}</p>
                                                    </div>
                                                </div>
                                                @enderror
                                                <div class="text-center mt-6">
                                                    <button class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full" type="submit" style="transition: all 0.15s ease 0s;">
                                                        Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <footer class="absolute w-full bottom-0 bg-gray-900 pb-6">
                <div class="container mx-auto px-4">
                    <hr class="mb-6 border-b-1 border-gray-700" />
                    <div class="flex flex-wrap items-center md:justify-between justify-center">
                        <div class="w-full md:w-4/12 px-4">
                            <div class="text-sm text-white font-semibold py-1">
                                Copyright © 2022
                                <a href="https://www.creative-tim.com" class="text-white hover:text-gray-400 text-sm font-semibold py-1">PLWEP</a>
                            </div>
                        </div>
                    </div>
            </footer>
        </section>
    </main>
</body>
<script>
    function toggleNavbar(collapseID) {
        document.getElementById(collapseID).classList.toggle("hidden");
        document.getElementById(collapseID).classList.toggle("block");
    }
</script>

</html>