<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;

class ProjectController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $projectDatas = Project::orderBy('id','desc')->paginate(5);
        return view('project.index', compact('projectDatas'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'year' => 'required',
            'role' => 'required',
            'project' => 'required',
            'description' => 'required',
        ]);
        
        Project::create($request->post());

        return redirect()->route('project.index');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\projects  $projects
    * @return \Illuminate\Http\Response
    */
    public function edit(Project $project)
    {
        return view('project.edit',compact('project'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\projects  $projects
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'year' => 'required',
            'role' => 'required',
            'project' => 'required',
            'description' => 'required',
        ]);
        
        $project->fill($request->post())->save();

        return redirect()->route('project.index');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\projects  $projects
    * @return \Illuminate\Http\Response
    */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route('project.index');
    }
}
