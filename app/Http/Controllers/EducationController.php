<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Education;

class EducationController extends Controller
{
        /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $educations = Education::orderBy('id','desc')->paginate(5);
        return view('education.index', compact('educations'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'year' => 'required',
            'educational' => 'required',
            'institution' => 'required',
        ]);
        
        Education::create($request->post());

        return redirect()->route('education.index');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\projects  $projects
    * @return \Illuminate\Http\Response
    */
    public function edit(Education $education)
    {
        return view('education.edit',compact('education'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\projects  $projects
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, Education $education)
    {
        $request->validate([
            'year' => 'required',
            'educational' => 'required',
            'institution' => 'required',
        ]);
        
        $education->fill($request->post())->save();

        return redirect()->route('education.index')->with('success','education Has Been updated successfully');
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\projects  $projects
    * @return \Illuminate\Http\Response
    */
    public function destroy(Education $education)
    {
        $education->delete();
        return redirect()->route('education.index');
    }
}
